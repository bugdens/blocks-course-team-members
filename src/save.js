import { useBlockProps, InnerBlocks } from '@wordpress/block-editor';

export default function save({ attributes }) {
	const { myColumns } = attributes;

	return (
		<div
			{...useBlockProps.save({
				className: `has-${myColumns}-columns`,
			})}
		>
			<InnerBlocks.Content />
		</div>
	);
}
