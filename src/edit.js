import {
	useBlockProps,
	InnerBlocks,
	InspectorControls,
} from '@wordpress/block-editor';
import { PanelBody, RangeControl } from '@wordpress/components';
import { __ } from '@wordpress/i18n';
import './editor.scss';

export default function Edit(props) {
	const { attributes, setAttributes } = props;
	const { myColumns } = attributes;

	const onChangeColumns = (newColumns) => {
		setAttributes({ myColumns: newColumns });
	};

	return (
		<div
			{...useBlockProps({
				className: `has-${myColumns}-columns`,
			})}
		>
			<InspectorControls>
				<PanelBody>
					<RangeControl
						label={__('Columns', 'team-members')}
						min={1}
						max={6}
						onChange={onChangeColumns}
						value={myColumns}
					/>
				</PanelBody>
			</InspectorControls>

			<InnerBlocks
				allowedBlocks={['blocks-course/team-member']}
				orientation="horizontal" // Set the orientation to horizontal as  the blocks are aligned horizontally and when dragged and dropped
				// this will cause a blue vertical line to appear indicating where the block will be dropped.
				template={[
					['blocks-course/team-member'],
					['blocks-course/team-member'],
					['blocks-course/team-member'],
				]}
				//templateLock="all"  //Prevent more items being added, 'insert' is also a valid option.
			/>
		</div>
	);
}
