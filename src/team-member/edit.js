import { useEffect, useState, useRef } from '@wordpress/element';
import {
	useBlockProps,
	RichText,
	MediaPlaceholder,
	BlockControls,
	MediaReplaceFlow,
	InspectorControls,
	store as blockEditorStore, //Import the store 'core/block-editor'
} from '@wordpress/block-editor';
import { isBlobURL, revokeBlobURL } from '@wordpress/blob';
import { __ } from '@wordpress/i18n';
import { useSelect } from '@wordpress/data';

//import the compose hook
import { usePrevious } from '@wordpress/compose';

import {
	Spinner,
	withNotices,
	ToolbarButton,
	PanelBody,
	TextareaControl,
	SelectControl,
	Icon, //Component used to display dashicons
	Tooltip,
	TextControl,
	Button,
} from '@wordpress/components';

//noticeOperations and noticeUI are added by the higher order 'withNotices' function.
function Edit({
	attributes,
	setAttributes,
	noticeOperations,
	noticeUI,
	isSelected, //tells us if the block is selected, provided in the props.
}) {
	const { name, bio, url, alt, id, socialLinks } = attributes;
	const [blobURL, setBlobURL] = useState();

	const [selectedLink, setSelectedLink] = useState();

	//Determine the previous url
	const prevURL = usePrevious(url);
	//Determine the previous isSelected value
	//I assume this works automatically just tracking the value of the parameter.
	const prevIsSelected = usePrevious(isSelected);

	//Create a ref we can associate with the title richtext box.
	const titleRef = useRef();

	const onChangeName = (newName) => {
		setAttributes({ name: newName });
	};
	const onChangeBio = (newBio) => {
		setAttributes({ bio: newBio });
	};
	const onChangeAlt = (newAlt) => {
		setAttributes({ alt: newAlt });
	};
	const onSelectImage = (image) => {
		if (!image || !image.url) {
			setAttributes({ url: undefined, id: undefined, alt: '' });
			return;
		}
		setAttributes({ url: image.url, id: image.id, alt: image.alt });
	};
	const onSelectURL = (newURL) => {
		setAttributes({
			url: newURL,
			id: undefined,
			alt: '',
		});
	};

	const onUploadError = (message) => {
		noticeOperations.removeAllNotices();
		noticeOperations.createErrorNotice(message);
	};

	//****************************************
	// Image sizes
	//****************************************

	const onChangeImageSize = (newURL) => {
		setAttributes({ url: newURL });
	};

	//This function access the following datastore:
	// wp.data.select('core').getMedia(10361) - where 10361 is the id of the image we're accessing
	//Apply the above to
	const imageObject = useSelect(
		(select) => {
			const { getMedia } = select('core'); //Deconstruct to get the getMedia function.
			return id ? getMedia(id) : null; //If we have an id, use the getMedia function to retrieve the details.
		},
		[id] // This is an array of dependencies.
	);

	//Determine the sizes defined in out current theme
	//wp.data.select('core/block-editor').getSettings().imageSizes
	const imageSizes = useSelect((select) => {
		return select(blockEditorStore).getSettings().imageSizes;
		//return select('core/block-editor').getSettings().imageSizes;
	}, []);

	const getImageSizeOptions = () => {
		if (!imageObject) return [];
		// console.log('imageObject');
		// console.log(imageObject);
		const options = [];
		const sizes = imageObject.media_details.sizes;
		//console.log('key');
		for (const key in sizes) {
			//console.log(key);
			const size = sizes[key];
			const imageSize = imageSizes.find((s) => s.slug === key);
			if (imageSize) {
				options.push({
					label: imageSize.name,
					value: size.source_url,
				});
			}
		}
		return options;
	};

	const removeImage = () => {
		setAttributes({
			url: undefined,
			alt: '',
			id: undefined,
		});
	};

	const addNewSocialItem = () => {
		setAttributes({
			//Copy the existing socialLinks array (...socialLinks) back into the socialLinks and then add an empty
			//item.
			socialLinks: [...socialLinks, { icon: '', link: '' }],
		});
		//Select the last item
		setSelectedLink(socialLinks.length);
	};

	const updateSocialItem = (type, value) => {
		const socialLinksCopy = [...socialLinks]; //Take a copy of the socialLinks
		socialLinksCopy[selectedLink][type] = value; //Set the relevant value in the copy
		setAttributes({ socialLinks: socialLinksCopy }); //Set the socialLinks with the copy
	};

	const removeSocialItem = () => {
		setAttributes({
			socialLinks: [
				//Slice loads a section of the array, two parameters specify which elements
				//We can remove the selectedLink by taking the elements before and after it.
				//so say we had 5 elements and wanted to remove the 3rd one i.e. index = 2
				//...socialLinks.slice(0, 2) retrieves the first two elements
				//...socialLinks.slice(selectedLink + 1) retrieves the elements after the 3rd one which we want to remove, since there's
				//only one parameter it will select from the specified index to the end.
				//the three dots before the socialLinks simply force a copy of the array to be taken.
				...socialLinks.slice(0, selectedLink),
				...socialLinks.slice(selectedLink + 1),
			],
		});
		setSelectedLink(); //Clear the selected link
	};

	//******************************************

	//This function clears any possible blob url that remains stored in the attributes
	//The blob url can be stored if the user refreshes the page before the image has loaded.
	//Clear the blob url if we don't have an id (in other words we don't havve an image) and the url is a blob url.
	//useEffect, a clean-up function runs before the component is removed from the UI to prevent memory leaks.
	useEffect(() => {
		if (!id && isBlobURL(url)) {
			setAttributes({
				url: undefined,
				alt: '',
			});
		}
	}, []); //Empty array of dependencies stops the function running everytime, just runs once on load I presume.

	//This function runs when the url changes, as indicated by the array containing  'url'
	//We first check to see if the url is a blob url and store it if so.
	//For the next change to the url, it's probably going to be the actual url, in which case we can remove the
	//memory the blobUrl is using by calling revokeBlobURL.
	//This has the effect of clearing the blob url from the browsers memory.
	useEffect(() => {
		if (isBlobURL(url)) {
			setBlobURL(url);
		} else {
			revokeBlobURL(blobURL);
			setBlobURL();
		}
	}, [url]);

	//Set focus to the title if upload an image.
	//Only set focus if we have a new url and an old url didn't exist.
	//In other words, only set focus the first time we add an image.
	useEffect(() => {
		if (url && !prevURL) {
			titleRef.current.focus();
		}
	}, [url, prevURL]);

	//Run this code when the isSelected, prevIsSelected values change
	//If this block was previously selected and no longer selected, clear the selectedLink and therefore remove the class and the red box which the class defines.
	useEffect(() => {
		if (prevIsSelected && !isSelected) {
			setSelectedLink();
		}
	}, [isSelected, prevIsSelected]);

	return (
		<>
			<InspectorControls>
				<PanelBody title={__('Image Settings', 'team-members')}>
					{id && (
						<SelectControl
							label={__('Image Size', 'team-members')}
							options={getImageSizeOptions()}
							value={url}
							onChange={onChangeImageSize}
						/>
					)}

					{url && !isBlobURL(url) && (
						<TextareaControl
							label={__('Alt Text', 'team-members')}
							value={alt}
							onChange={onChangeAlt}
							help={__(
								"Alternative text describes your image to people can't see it. Add a short description with its key details.",
								'team-members'
							)}
						/>
					)}
				</PanelBody>
			</InspectorControls>

			{/* //Put the controls with the 'inline' controls, where inline is a group of controls in the toolbar apparently */}
			{url && (
				<BlockControls group="inline">
					{/* The <MediaReplaceFlow> will display a replace button so the image can be changed. */}
					<MediaReplaceFlow
						name={__('Replace Image', 'team-members')}
						onSelect={onSelectImage}
						onSelectURL={onSelectURL}
						onError={onUploadError}
						accept="image/*"
						allowedTypes={['image']}
						mediaId={id} //Means the current image will be selected when accessing the media library
						mediaURL={url} //Means the current image will be selected when accessing the media library
					/>
					<ToolbarButton onClick={removeImage}>
						{__('Remove Image', 'team-members')}
					</ToolbarButton>
				</BlockControls>
			)}
			<div {...useBlockProps()}>
				{url && (
					<div
						// Add 'is-loading' to the class name if we have a blob url
						className={`wp-block-blocks-course-team-member-img${
							isBlobURL(url) ? ' is-loading' : ''
						}`}
					>
						{/* Only display spinner if we have a blob url */}
						<img src={url} alt={alt} />
						{isBlobURL(url) && <Spinner />}
					</div>
				)}

				<MediaPlaceholder
					icon="admin-users"
					onSelect={onSelectImage}
					onSelectURL={onSelectURL}
					onError={onUploadError}
					accept="image/*" //Only allow images
					allowedTypes={['image']} //Only allow images
					disableMediaButtons={url} //hide MediaPlaceholder if there is an image selected.
					notices={noticeUI} //Display notices provided by the higher order component 'withNotices'
				/>
				<RichText
					ref={titleRef} //Assign the reference, so we can manipulate the control.
					placeholder={__('Member Name', 'team-member')}
					tagName="h4"
					onChange={onChangeName}
					value={name}
					allowedFormats={[]} // no specified formats, means no formatting such as bold
				/>
				<RichText
					placeholder={__('Member Bio', 'team-member')}
					tagName="p"
					onChange={onChangeBio}
					value={bio}
					allowedFormats={[]}
				/>

				<div className="wp-block-blocks-course-team-member-social-links">
					<ul>
						{socialLinks.map((item, index) => {
							return (
								<li
									key={index}
									className={
										selectedLink === index
											? 'is-selected'
											: null
									}
								>
									<button
										aria-label={__(
											'Edit Social Link',
											'team-members'
										)}
										onClick={() => setSelectedLink(index)}
									>
										<Icon icon={item.icon} />
									</button>
								</li>
							);
						})}
						{/* Only display the social media add block if the team member block is selected, the isSelected property 
						is obtained in the parameter list, I assume it's just one of those properties that's supplied automatically */}
						{isSelected && (
							<li className="wp-block-blocks-course-team-member-add-icon-li">
								{/* Tool tip wraps the button to provide a tooltip */}
								<Tooltip
									text={__('Add Social Link', 'team-members')}
								>
									<button
										aria-label={__(
											'Add Social Link',
											'team-members'
										)}
										onClick={addNewSocialItem}
									>
										<Icon icon="plus" />
									</button>
								</Tooltip>
							</li>
						)}
					</ul>
				</div>
				{selectedLink !== undefined && (
					<div className="wp-block-blocks-course-team-member-link-form">
						<TextControl
							label={__('Icon', 'text-members')}
							value={socialLinks[selectedLink].icon}
							onChange={(icon) => {
								updateSocialItem('icon', icon);
							}}
						/>
						<TextControl
							label={__('URL', 'text-members')}
							value={socialLinks[selectedLink].link}
							onChange={(link) => {
								updateSocialItem('link', link);
							}}
						/>
						<br />
						<Button isDestructive onClick={removeSocialItem}>
							{/* isDestructive property will make the button red */}
							{__('Remove Link', 'text-members')}
						</Button>
					</div>
				)}
			</div>
		</>
	);
}

export default withNotices(Edit);
